#include<iostream> 
#include<iomanip>
using namespace std;

void WaitTime_RR(int Process_No[], int burstT[], int waitT[], int quantum_value);
void TurnAroundTime_RR(int Process_No[], int burstT[], int waitT[], int TurnAT[]);
void avgTimeRR(int Process_No[], int burstT[], int quantum_value);
void WaitTime(int Process_No[], int burstT[], int waitT[]);
void TurnAroundTime(int Process_No[], int burstT[], int waitT[], int TurnAT[]);
void avgTime(int Process_No[], int burstT[]);

int main()
{
	// Quantum value
	int quantum_value = 39;
	// Process number 
	int Process_No[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100 };

	//Burst time of all processes
	
	//Test 1 S:50% M:20% L:30% (ms)
	//int burstT[] = { 5, 22, 20, 26, 4, 5, 39, 3, 29, 39, 3, 4, 8, 6, 5, 38, 38, 26, 8, 39, 2, 5, 36, 5, 39, 3, 4, 6, 7, 37, 3, 30, 35, 40, 35, 3, 36, 8, 28, 4, 35, 30, 37, 28, 3, 28, 3, 7, 24, 36, 5, 6, 5, 7, 27, 38, 38, 25, 8, 36, 7, 3, 2, 4, 7, 2, 8, 35, 38, 37, 40, 4, 35, 21, 4, 4, 38, 37, 40, 26, 7, 8, 7, 25, 8, 35, 20, 38, 35, 2, 29, 35, 2, 20, 7, 5, 6, 6, 8, 21 };
	//int burstT2[] = { 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 20, 20, 20, 21, 21, 22, 24, 25, 25, 26, 26, 26, 27, 28, 28, 28, 29, 29, 30, 30, 35, 35, 35, 35, 35, 35, 35, 35, 36, 36, 36, 36, 37, 37, 37, 37, 38, 38, 38, 38, 38, 38, 38, 39, 39, 39, 39, 40, 40, 40 };
	
	//Test 2 S:30% M:50% L:20% (ms)
	//int burstT[] = { 24, 29, 36, 22, 23, 23, 2, 28, 29, 35, 24, 2, 39, 22, 24, 26, 39, 8, 8, 35, 30, 2, 22, 29, 25, 7, 6, 37, 4, 26, 26, 37, 7, 26, 3, 7, 8, 25, 36, 2, 29, 29, 36, 38, 40, 28, 23, 7, 23, 24, 23, 3, 40, 24, 25, 23, 36, 8, 2, 39, 5, 36, 29, 29, 25, 28, 23, 4, 3, 29, 37, 8, 29, 37, 6, 26, 2, 39, 4, 21, 3, 26, 23, 26, 25, 28, 6, 5, 28, 2, 20, 27, 26, 37, 8, 24, 22, 2, 21, 39 };
	//int burstT2[] = { 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 20, 21, 21, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 27, 28, 28, 28, 28, 28, 29, 29, 29, 29, 29, 29, 29, 29, 29, 30, 35, 35, 36, 36, 36, 36, 36, 37, 37, 37, 37, 37, 38, 39, 39, 39, 39, 39, 40, 40 };
	
	//Test 3 S:20% M:30% L:50% (ms)
	int burstT[] = { 7, 40, 36, 37, 35, 40, 21, 36, 39, 36, 2, 25, 40, 36, 3, 29, 35, 39, 7, 38, 40, 35, 28, 8, 2, 35, 29, 37, 20, 27, 8, 28, 2, 36, 36, 40, 35, 25, 38, 21, 21, 40, 40, 7, 24, 3, 30, 36, 23, 36, 27, 36, 40, 5, 40, 7, 6, 27, 38, 36, 23, 26, 40, 23, 5, 35, 8, 35, 35, 35, 38, 37, 35, 27, 7, 26, 4, 36, 29, 38, 40, 39, 20, 36, 40, 37, 35, 35, 29, 28, 24, 29, 27, 35, 3, 6, 35, 7, 21, 30 };
	int burstT2[] = { 2, 2, 2, 3, 3, 3, 4, 5, 5, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 20, 20, 21, 21, 21, 21, 23, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 27, 27, 27, 28, 28, 28, 29, 29, 29, 29, 29, 30, 30, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 37, 37, 37, 37, 38, 38, 38, 38, 38, 39, 39, 39, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};
	
	cout << "FCFS algorithm" << endl;
	avgTime(Process_No, burstT);
	cout << "SJF algorithm" << endl;
	avgTime(Process_No, burstT2); 
	cout << "RR algorithm" << endl;
	avgTimeRR(Process_No, burstT, quantum_value); 
	
	return 0;
}

//Find all processes waiting time of RR algorithm
void WaitTime_RR(int Process_No[], int burstT[], int waitT[], int quantum_value)
{
	int RemainBT[100]; //Copy burst time for store remaining
	int TimeCur = 0; // Current time 

	for (int i = 0; i < 100; i++)
	{
		RemainBT[i] = burstT[i];
	}

	//Traversing processes until all of them are not finish. 
	while (true)
	{
		bool Finish = true;
		//Traverse all processes one by one
		for (int i = 0; i < 100; i++)
		{
			//If burst time remain need to do again
			if (RemainBT[i] > 0)
			{
				Finish = false; //Pending process 
				if (RemainBT[i] > quantum_value)
				{
					// Increase the value of TimeCur by timeslide of quantum
					TimeCur = TimeCur + quantum_value;
					// Decrease the burst_time of current process by quantum 
					RemainBT[i] = RemainBT[i] - quantum_value;
				}
				// If burst time is smaller than or equal to 
				// quantum. Last cycle for this process 
				else
				{
					// how much time a process has been processed 
					TimeCur = TimeCur + RemainBT[i];
					// Waiting time is current time minus time used by this process 
					waitT[i] = TimeCur - burstT[i];
					//Process finish
					RemainBT[i] = 0;
				}
			}
		}
		// If all processes are finished
		if (Finish == true)
		{
			break;
		}
	}
}

//Find turn around time of RR algorithm
void TurnAroundTime_RR(int Process_No[], int burstT[], int waitT[], int TurnAT[])
{
	//Turnaround time finding by adding burst time and waiting time
	for (int i = 0; i < 100; i++)
	{
		TurnAT[i] = burstT[i] + waitT[i];
	}
}

//Find average time of RR algorithm
void avgTimeRR(int Process_No[], int burstT[], int quantum_value)
{
	int waitT[100], TurnAT[100], total_waitT = 0, total_TurnAT = 0;
	WaitTime_RR(Process_No, burstT, waitT, quantum_value);
	TurnAroundTime_RR(Process_No, burstT, waitT, TurnAT);

	//Show result
	cout << setw(10) << "Processes" << setw(12) << "Burst time" << setw(14) << "Waiting time" << setw(18) << "Turn around time\n";

	//Find total waiting time and total turn around time
	for (int i = 0; i < 100; i++)
	{
		total_waitT = total_waitT + waitT[i];
		total_TurnAT = total_TurnAT + TurnAT[i];
		cout << setw(5) << i + 1 << setw(12) << burstT[i] << setw(14) << waitT[i] << setw(15) << TurnAT[i] << endl;
	}
	cout << "Average waiting time = " << (float)total_waitT / 100;
	cout << "\nAverage turn around time = " << (float)total_TurnAT / 100;
	cout << endl << endl;
}

//Find all processes waiting time of SJF/FCFS algorithm
void WaitTime(int Process_No[], int burstT[], int waitT[])
{
	//First process wait time is 0 
	waitT[0] = 0;
	for (int i = 1; i < 100; i++)
	{
		waitT[i] = burstT[i - 1] + waitT[i - 1];
	}
}

//Find turn around time of SJF/FCFS algorithm
void TurnAroundTime(int Process_No[], int burstT[], int waitT[], int TurnAT[])
{
	//Turnaround time finding by adding burst time and waiting time
	for (int i = 0; i < 100; i++)
	{
		TurnAT[i] = burstT[i] + waitT[i];
	}
}

//Find average time of SJF/FCFS algorithms
void avgTime(int Process_No[], int burstT[])
{
	int waitT[100], TurnAT[100], total_waitT = 0, total_TurnAT = 0;
	WaitTime(Process_No, burstT, waitT);
	TurnAroundTime(Process_No, burstT, waitT, TurnAT);

	//Show result
	cout << setw(10) <<"Processes" << setw(12) << "Burst time" << setw(14) << "Waiting time" << setw(18) << "Turn around time\n";

	//Find total waiting time and total turn around time
	for (int i = 0; i < 100; i++)
	{
		total_waitT = total_waitT + waitT[i];
		total_TurnAT = total_TurnAT + TurnAT[i];
		cout << setw(5) <<i + 1  <<  setw(12)<< burstT[i] <<setw(14) << waitT[i] << setw(15) << TurnAT[i] << endl;
	}
	cout << "Average waiting time = " << (float)total_waitT / 100;
	cout << "\nAverage turn around time = " << (float)total_TurnAT / 100;
	cout << endl << endl;
}
